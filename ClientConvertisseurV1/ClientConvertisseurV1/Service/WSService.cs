﻿using ClientConvertisseurV1.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ClientConvertisseurV1.Service
{
    public sealed class WSService
    {
        private static readonly WSService instance = new WSService();
        private static HttpClient client = null;

        static WSService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:5000/api/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public async Task<List<Devise>> GetAllDevisesAsync()
        {
            // Call asynchronous network methods in a try/catch block to handle exceptions.
            try
            {
                HttpResponseMessage response = await client.GetAsync("Devise");
                if (response.IsSuccessStatusCode)
                {
                    List<Devise> listDevise = await response.Content.ReadAsAsync<List<Devise>>();
                    return listDevise;
                }
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
            return null;
        }

        public static WSService getInstance()
        {
            return instance;
        }
    }
}
