﻿using ClientConvertisseurV1.Models;
using ClientConvertisseurV1.Service;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ClientConvertisseurV1
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        public MainPage()
        {
            this.InitializeComponent();
            ActionGetData();
        }
        private async void ActionGetData()
        {
            try
            {
            WSService wSService = WSService.getInstance();
            var result = await wSService.GetAllDevisesAsync();
                this.cb_devise.DataContext = new List<Devise>(result);
            }
            catch (Exception)
            {
                createMessageDialog("erreur lors du cahrgement des devises.");
            }
        }


        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void bt_Convertir_Click(object sender, RoutedEventArgs e)
        {
            double montantInit; 
            Devise devise;

            var match = Regex.Match(txtBox_montantInit.Text, "^[0-9]+(\\.[0-9]+)?$", RegexOptions.None);

            if (match.Success && cb_devise.SelectedItem is Devise)
            {
                if (cb_devise.SelectedItem is Devise)
                {
                    devise = (Devise)(cb_devise.SelectedItem);
                    montantInit = double.Parse(txtBox_montantInit.Text);
                    txtBox_montantFin.Text = (montantInit * devise.Rate).ToString();
                }
                else
                {
                    createMessageDialog("L'bojet séléctionné dans la comboBo xn'est pas une devise.");
                }
            }
            else
            {
                createMessageDialog("Montant invalide.");
                txtBox_montantFin.Text = "";
            }
        }

        private async void createMessageDialog(string str)
        {
            MessageDialog messageDialog = new MessageDialog(str);
            await messageDialog.ShowAsync();
        }

    }

}
