﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WsConvertisseur.Models
{
    public class Devise
    {

        private int id;
        [Required]
        public int Id { get; set; }

        
        private String name;
        [Required]
        public String Name { get; set; }

        private double rate;
        public double Rate { get; set; }

        public Devise()
        {

        }

        public Devise(int id, string name, double rate)
        {
            Id = id;
            Name = name;
            Rate = rate;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "Id : " + Id + " ~~ Name : " + Name + " ~~ Rate: " + Rate ;
        }
    }

}
