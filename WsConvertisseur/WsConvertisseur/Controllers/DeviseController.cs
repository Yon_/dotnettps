﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WsConvertisseur.Models;

namespace WsConvertisseur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviseController : ControllerBase
    {

        private List<Devise> listDevise;

      
        public DeviseController()
        {
            listDevise = new List<Devise>();

            listDevise.Add(new Devise(1,"Dollar",1.08));
            listDevise.Add(new Devise(2,"Franc suisse",1.07));
            listDevise.Add(new Devise(3,"Yen",120));
        }

        // GET: api/Devise
        /// <summary>
        /// Return the list of all devise
        /// </summary>
        [ProducesResponseType(typeof(IEnumerable<Devise>), 200)]
        [HttpGet]
        public IEnumerable<Devise> GetAll()
        {
            return listDevise;
        }

        // GET: api/Devise/5
        /// <summary>
        /// Get a single currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="id">The id of the currency</param>
        /// <response code="200">When the currency id is found</response>
        /// <response code="404">When the currency id is not found</response>
        [ProducesResponseType(typeof(Devise), 200)]
        [ProducesResponseType(404)]
        [HttpGet("{id}", Name = "GetDevise")]
        public IActionResult GetById(int id)
        {
            Devise devise = listDevise.FirstOrDefault((d) => d.Id == id);
            /*Devise devise = (from d in listDevise
                    where d.Id == id
                    select d).FirstOrDefault();*/

            if (devise == null)
                return NotFound();
            else
                return Ok(devise);
        }

        // POST: api/Devise
        /// <summary>
        /// Add a single currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="devise">The currency</param>
        /// <response code="200">When the currency is succefully added</response>
        /// <response code="400">When the devise is not formated well </response>
        [ProducesResponseType(typeof(Devise), 201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public IActionResult Post([FromBody]Devise devise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            listDevise.Add(devise);
            return CreatedAtRoute("GetDevise", new { id = devise.Id }, devise);
            //return CreatedAtAction("GetById", new { id = devise.id }, devise);
        }

        // PUT: api/Devise/5
        /// <summary>
        /// Update a single currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="devise">The currency</param>
        /// <response code="204">When the currency is succefully updated</response>
        /// <response code="400">When the devise is not in a good format </response>
        /// <response code="404">When the devise is not found </response>
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Devise devise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != devise.Id)
            {
                return BadRequest();
            }
            int index = listDevise.FindIndex((d) => d.Id == id);
            if (index < 0)
            {
                return NotFound();
            }
            listDevise[index] = devise;
            return NoContent();
        }


        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// Delete a single currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="id">The id of the currency</param>
        /// <response code="200">When the currency id is deleted</response>
        /// <response code="404">When the currency id is not found</response>
        [ProducesResponseType(typeof(Devise), 200)]
        [ProducesResponseType(404)]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Devise devise = (from d in listDevise
                    where d.Id == id
                    select d).FirstOrDefault();

            if (devise == null)
                return NotFound();
            else
            {
                listDevise.Remove(devise);
                return Ok(devise);
            }
                

        }
    }
}
